const routes = module.exports = require('next-routes')()

routes.add('mainHomePage', '/', 'mainHomePage');
routes.add('errorPage', '/error', 'errorPage');
