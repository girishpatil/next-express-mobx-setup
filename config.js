config = {
    "appName": 'APP_NAME',
    "mongoDb": process.env.NODE_ENV === 'production' ? 'mongodb://localhost/test' : 'mongodb://localhost/test',
    "port" : process.env.NODE_ENV === 'production' ? 8080 : 8080,
    "baseURL" : process.env.NODE_ENV === 'production' ? 'http://0.0.0.0:8080' : 'http://localhost:8080',
}

module.exports = config;