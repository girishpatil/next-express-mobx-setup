const next = require('next');
const config = require('./config');
const mongoose = require('mongoose');
const bluebird = require('bluebird');
const passport = require('passport');

const body = require('body-parser');
const express = require('express');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const routes = require('./routes');

const dev = process.env.NODE_ENV === 'development';
const app = next({ dev });

const MONGO_URL = config.mongoDb;
const PORT = config.port;

const handle = routes.getRequestHandler(app);

app.prepare().then(() => {
	mongoose.Promise = bluebird;
	console.log(MONGO_URL);
	mongoose.connect(MONGO_URL);

	const server = express();

	server.disable('x-powered-by');
	server.set('trust proxy', 1);
	server.use(cookieParser());
	server.use(
		session({
		secret: 'secret',
		resave: true,
		saveUninitialized: true,
		cookie: { httpOnly: true },
		})
	);

	server.use(body.json({ limit: '20mb' }));
	server.use(body.urlencoded({ limit: '20mb', extended: false }));

	// Add csrf middleware
	if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {
		server.use(csrf({ cookie: true }));
	}

	// Initialize passport
	server.use(passport.initialize());
	server.use(passport.session());

	passport.serializeUser(function(user, done) {
		done(null, user);
	});

	passport.deserializeUser(function(user, done) {
		done(null, user);
	});

	//   Add other routes

	server.use((req, res) => {
		return handle(req, res);
	});

	server.listen(PORT);
	console.log(`Listening on ${PORT}`);

});