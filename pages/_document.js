// ./pages/_document.js
import Document, { Head, Main, NextScript } from 'next/document';

export default class APp extends Document {

  constructor(props, context) {
      super(props, context);
  }

  static getInitialProps({ renderPage, req }) {
    const { html, head, errorHtml, chunks } = renderPage();
    const csrfToken = req.csrfToken && req.csrfToken();
    const path = req.path;
    return { html, head, errorHtml, chunks, csrfToken, path }
  }

  render() {

    return (
      <html itemScope itemType="http://schema.org/WebSite" lang="en">
        <Head>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <meta name="twitter:site" content="" />
            <meta property="fb:app_id" content=""/>
            <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"/>
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
            <link rel="stylesheet" type="text/css" href="/static/styles/app.css" />
            <link rel="icon" type="image/png" sizes="32x32" href=""/>
            <link rel="icon" type="image/png" sizes="16x16" href=""/>
            {/* Favicons             */}
            {/* <link rel="apple-touch-icon" sizes="180x180" href=""/>
            <link rel="icon" type="image/png" sizes="32x32" href=""/>
            <link rel="icon" type="image/png" sizes="16x16" href=""/>
            <link rel="manifest" href=""/>
            <link rel="mask-icon" href="" color=""/>
            <link rel="shortcut icon" href=""/>
            <meta name="msapplication-TileColor" content=""/>
            <meta name="msapplication-config" content=""/>
            <meta name="theme-color" content=""/> */}
        </Head>
        <body>
            <Main />
            <input type="hidden" value={this.props.csrfToken} id="csrfToken" />
            <NextScript />
        </body>
      </html>
    )
  }
}
